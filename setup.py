import setuptools

with open("README.md", "r") as fh:
    long_description = fh.read()

setuptools.setup(
    name='lab_helpers',
    version='0.1',
    description='A package providing functions to use for the analysis of scientific lab data.',
    long_description=long_description,
    long_description_content_type="text/markdown",
    url='https://marcelhohn@bitbucket.org/marcelhohn/lab_helpers.git',
    author='Marcel Hohn',
    author_email='marcel.hohn@web.de',
    license='MIT',
    packages=setuptools.find_packages(),
    classifiers=[
        "Programming Language :: Python :: 3",
        "License :: MIT License",
        "Operating System :: OS Independent",
    ],
    install_requires=[
        'markdown',
        'scipy'
      ],
)
