import csv
import numpy as np


def import_data(path, delimiter=',', comment_marker='#', x_col=0, y_col=1, xerr_col=None, yerr_col=None):
    x = []
    y = []
    xerr = []
    yerr = []
    columns = [x_col, y_col, xerr_col, yerr_col]
    magnitudes = [x, y, xerr, yerr]
    with open(path, 'r') as input_data:
        rows = csv.reader(input_data, delimiter=delimiter)
        for row in rows:
            if row[0][0] != comment_marker:
                for i in range(len(columns)):
                    if columns[i] is not None:
                        magnitudes[i].append(float(row[columns[i]]))
    result = None
    for i in range(len(magnitudes)):
        if len(magnitudes[i]) > 0:
            if result is None:
                result = np.array(magnitudes[i])
            else:
                result = np.stack((result, np.array(magnitudes[i])))
    return result


def import_table(path, delimiter=',', comment_marker='#', transpose=False, dtype=None):
    table = np.array([])

    #read file
    with open(path, 'r') as input_data:
        rows = csv.reader(input_data, delimiter=delimiter)
        for row in rows:
            if row[0][0] != comment_marker:
                if not len(table):
                    table = np.array(row, dtype=dtype)
                else:
                    table = np.vstack((table, np.array(row, dtype=dtype)))
    #transpose
    if transpose:
        table = np.transpose(table)

    return table
