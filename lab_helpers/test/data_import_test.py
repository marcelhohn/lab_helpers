from data_import import *

import unittest as ut
import numpy as np


class DataImportTest(ut.TestCase):
    @staticmethod
    def test_import_file_with_only_y():
        expected_output = np.array([1.0, 3.0, 5.0])
        input_path = "test_data/only_y.csv"
        np.testing.assert_equal(import_data(input_path,
                                            delimiter='\t',
                                            x_col=None,
                                            y_col=0),
                                expected_output)

    def test_import_file_without_errors(self):
        expected_output = np.array([[0.5, 1.2, -5.0], [1.0, 3.0, 2.1]])
        input_path = "test_data/x_and_y.csv"
        np.testing.assert_equal(import_data(input_path, delimiter='\t'),
                                expected_output)
