from fitting import *
from functions import linear

import unittest as ut
import numpy as np


class ReducedChisquareTest(ut.TestCase):
    def test_reduced_chisquare_for_different_errors(self):
        yfit = np.array([0.327272716932313, 0.990909087559065, 1.65454545818582, 2.31818182881257])
        ydata = np.array([0, 1.1, 1.9, 2.1])
        y_err = np.array([0.2, 0.1, 0.3, 0.2])
        reduced_chisquare_from_gnuplot = 2.86364
        free_params = 2

        dof = len(ydata) - free_params
        chisqaure_result = round(reduced_chisquare(ydata, yfit, dof, yerr=y_err), 5)

        self.assertEqual(reduced_chisquare_from_gnuplot, chisqaure_result)

    def test_reduced_chisquare_without_errors(self):
        yfit = np.array([-1, 0, 1, 2, 3])
        ydata = np.array([-1.1, 0.1, 1, 2.1, 2.9])
        reduced_chisquare_from_gnuplot = 0.0133333
        free_params = 2

        dof = len(ydata) - free_params
        chisqaure_result = round(reduced_chisquare(ydata, yfit, dof), 7)

        self.assertEqual(reduced_chisquare_from_gnuplot, chisqaure_result)

    def test_reduced_chisquare_with_equal_errors(self):
        yfit = np.array([-1, 0, 1, 2, 3])
        ydata = np.array([-1.1, 0.1, 1, 2.1, 2.9])
        y_err = 0.1
        reduced_chisquare_from_gnuplot = 1.33333
        free_params = 2

        dof = len(ydata) - free_params
        chisqaure_result = round(reduced_chisquare(ydata, yfit, dof, yerr=y_err), 5)

        self.assertEqual(reduced_chisquare_from_gnuplot, chisqaure_result)


class ReducedChisquareFunctionTest(ut.TestCase):
    def test_reduced_chisquare_for_different_error(self):
        func = linear
        params = [0.663636, 0.990909]
        xdata = np.array([-1, 0, 1, 2])
        ydata = np.array([0, 1.1, 1.9, 2.1])
        y_err = np.array([0.2, 0.1, 0.3, 0.2])
        reduced_chisquare_from_gnuplot = 2.86364

        chisqaure_result = round(reduced_chisquare_func(func, params, xdata, ydata, y_obs_err=y_err), 5)

        self.assertEqual(reduced_chisquare_from_gnuplot, chisqaure_result)

    def test_reduced_chisquare_without_error(self):
        func = linear
        params = [1, 1]
        xdata = np.array([-2, -1, 0, 1, 2])
        ydata = np.array([-1.1, 0.1, 1, 2.1, 2.9])
        reduced_chisquare_from_gnuplot = 0.0133333

        chisqaure_result = round(reduced_chisquare_func(func, params, xdata, ydata), 7)

        self.assertEqual(reduced_chisquare_from_gnuplot, chisqaure_result)

    def test_reduced_chisquare_function_with_equal_errors(self):
        func = linear
        params = [1, 1]
        xdata = np.array([-2, -1, 0, 1, 2])
        ydata = np.array([-1.1, 0.1, 1, 2.1, 2.9])
        y_err = 0.1
        reduced_chisquare_from_gnuplot = 1.33333

        chisqaure_result = round(reduced_chisquare_func(func, params, xdata, ydata, y_obs_err=y_err), 5)

        self.assertEqual(reduced_chisquare_from_gnuplot, chisqaure_result)


class GetFitBoundIdxTest(ut.TestCase):
    def test_get_fit_bound_idx_bounds_not_in_values(self):
        values = [0, 1, 2, 3, 4]
        bounds = [0.5, 2.5]
        expected_idx = [1, 2]

        self.assertEqual(get_fit_bound_idx(values,bounds),expected_idx)

    def test_get_fit_bound_idx_bounds_in_values(self):
        values = [0, 1, 2, 3, 4]
        bounds = [1, 3]
        expected_idx = [1, 3]

        self.assertEqual(get_fit_bound_idx(values,bounds),expected_idx)