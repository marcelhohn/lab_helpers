from scipy.optimize import curve_fit
import numpy as np


def reduced_chisquare(ydata, yfit, dof, yerr=1.0):
    chisquare = np.sum(((ydata - yfit)/yerr)**2)
    return chisquare / dof


def reduced_chisquare_func(fit_func: 'function',
                           params: list,
                           x_obs: np.ndarray,
                           y_obs: np.ndarray,
                           y_obs_err=1.0):
    """ Returns the reduced chi square of a fitted function.

    fit_func: function to fit,
    params: numpy array containing parameter values obtained from the fit,
    x_obs: observed x values,
    y_obs: observed y values,
    y_obs_err: standard deviation of observed y values. Can be either an array, if
    errors of individual measurements are different or just one number if they are the same. Can
    be left out if errors are not given.
    """
    y_fit = fit_func(x_obs, *params)
    n_params = len(params)
    dof = len(x_obs)-n_params
    return reduced_chisquare(y_fit, y_obs, dof, y_obs_err)


def get_fit_bound_idx(values, bounds: list):
    # assume sorted values (from low to high)
    idx_low = -1
    idx_high = -1
    for idx, value in enumerate(values):
        if value >= bounds[0]:
            idx_low = idx
            break
    for idx, value in enumerate(list(reversed(values))):
        if value <= bounds[1]:
            idx_high = len(values)-1-idx
            break
    return[idx_low, idx_high]


def fit(func, xdata, ydata, yerr=None, boundaries=None, init=None):

    if boundaries is not None:
        bounds_idx = get_fit_bound_idx(xdata, boundaries)
        xdata = xdata[bounds_idx[0]:bounds_idx[1]]
        ydata = ydata[bounds_idx[0]:bounds_idx[1]]
    p_opt, p_cov = curve_fit(
        func,
        xdata,
        ydata,
        yerr,
        absolute_sigma=False if yerr is None else True,
        p0=init)
    p_err = np.sqrt(np.diag(p_cov))
    dof = len(xdata) - len(p_opt)
    fit_chisquare = reduced_chisquare(ydata, func(xdata, *p_opt), dof, yerr)
    return np.array([p_opt, p_err, fit_chisquare])
