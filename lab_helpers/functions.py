from scipy import special
from math import sqrt


def linear(x, m, b):
    num = [int, float]
    if type(m) not in num:
        raise TypeError('m must be a real number, not', type(m))
    if type(b) not in num:
        raise TypeError('b must be a real number, not', type(m))
    return m * x + b


def erf(x, a, mu, sigma, c):
    return a/2 * special.erf((x-mu)/sqrt(2 * sigma ** 2)) + c